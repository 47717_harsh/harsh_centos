#include <stdio.h>
#include <pthread.h>
#include <semaphore.h>
int sum(int, int);
int counter = 0;  //Shared Variable
sem_t sem;
/*
    Thread 1 routine
*/
void *count1(void *pv)
{
        int index;
        sem_wait(&sem);
        counter++;
        printf("Inside the count1 routine; Start value of counter = %d\n",counter);
        for(index=0;index < 0xffffffff; index++);
        printf("Inside the count1 routine; End value of counter = %d\n",counter);
        sem_post(&sem);
}
/*
        Thread 2 Routine
*/
void *count2(void *pv)
{
    int index;
    sem_wait(&sem);
    counter++;
    printf("Inside the count2 routine; Start value of counter = %d\n",counter);
    for(index=0;index < 0xffffffff; index++);
    printf("Inside the count2 routine; End value of counter = %d\n",counter);
    int num1, num2;
    printf("Enter Number 1 : ");
    scanf("%d",&num1);
    printf("Enter Number 2 : ");
    scanf("%d",&num2);
    sum(num1,num2);
    sem_post(&sem);
}

int sum(int num1, int num2)
{
    int result = num1 + num2;
    printf("%d + %d = %d",num1,num2,result);
}

int main()
{
    pthread_t thread1, thread2;
    sem_init(&sem,0,1);  // 0 - stands for semaphore will be used between threads and 1 is initial value of semaphore
    pthread_create(&thread1,NULL,count1,NULL);
    pthread_create(&thread2,NULL,count2,NULL);
    pthread_join(thread1,NULL);
    pthread_join(thread2,NULL);
    sem_destroy(&sem);
    //printf("Value of counter = %d\n",counter);
    return 0;

}