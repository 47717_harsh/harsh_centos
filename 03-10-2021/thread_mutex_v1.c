#include <stdio.h>
#include <pthread.h>

//Ref : https://man7.org/linux/man-pages/man3/pthread_mutex_lock.3p.html
int counter = 0;  //Shared Variable
pthread_mutex_t lock;
/*
    Thread 1 routine
*/
void *count1(void *pv)
{
    int index;
        pthread_mutex_lock(&lock);
        counter++;
        printf("Inside the count1 routine; Start value of counter = %d\n",counter);
        for(index=0;index < 0xffffffff; index++);
        printf("Inside the count1 routine; End value of counter = %d\n",counter);
        pthread_mutex_unlock(&lock);
}
/*
        Thread 2 Routine
*/
void *count2(void *pv)
{
    int index;
    pthread_mutex_lock(&lock);
    counter++;
    printf("Inside the count2 routine; Start value of counter = %d\n",counter);
    for(index=0;index < 0xffffffff; index++);
    printf("Inside the count2 routine; End value of counter = %d\n",counter);
    pthread_mutex_unlock(&lock);
}

int main()
{
    pthread_t thread1, thread2;
    pthread_create(&thread1,NULL,count1,NULL);
    pthread_create(&thread2,NULL,count2,NULL);
    pthread_join(thread1,NULL);
    pthread_join(thread2,NULL);
    //printf("Value of counter = %d\n",counter);
    return 0;

}